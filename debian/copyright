Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: yt
Upstream-Contact: Ben Keller <malzraa@gmail.com>
Source: https://yt-project.org/

Files: *
Copyright: Anne M. Archibald 2008,
  (c) Damian Eads, 2007-2008,
  (c) 2007-2011 John Tsiombikas <nuclear@siggraph.org>,
  (c) 2006-2013, Matthew Turk <matthewturk@gmail.com>,
  (c) 2013-, yt Development Team,
  2015 Kacper Kowalik <xarthisius.kk@gmail.com> (Debian files)
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 * Neither the name of the Astropy Team nor the names of its contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: yt/frontends/artio/artio_headers/*
Copyright: (c) 2012-2013, Douglas H. Rudd
License: LGPL-3
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 . 
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.
Comment: This code is derived from knee.py, which was included in the Python
  2.6 distribution.

Files: yt/utilities/lib/ewahboolarray/*
Copyright: Daniel Lemire
License: Apache-2.0
 On Debian systems, the full text of The Apache Software License
 version 2 can be found in the file `/usr/share/common-licenses/Apache-2'.
